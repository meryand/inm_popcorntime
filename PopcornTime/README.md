## Synopsis

INM Tech Test

A simple iOS app to display a list of movies using themoviedb.org API and to allow the user to get more details about each movie she clicks onto.

•	An initial screen showing a button to request the list;
•	A second screen showing the list of movies returned by the request;
•	A third screen showing the details of the movie selected by the user in the second screen.

## Code & Architecture

I have created a folder structure to allocate the different classes.

- Helper: DateFormat (releaseYear), GenreParse, JSONParser and NetworkOperations (all HTTP request).

* MVVM Architecture *
- Model: Movie
- ViewController: HomeViewController, MoviesTableViewController, MovieDetailsTableViewCell, DetailsMoviewViewController
- ViewModel: MovieDetailsViewModel
- DataController: MoviesDataController

- Storyboards
- Resources: Images and plist files

As you can see there area a MVVM (Model-View-ViewModel) structure where the code has been separated following that architecture pattern.
That helps to keep the code as much detached as possible then it is easier for others to use that piece of code, re-use it everywerhe or even update it without a mayor impact.

## 3rd party libraries 

Added the library AFNetworking using Cocoapods to do all the HHTP requests for the themoviedb.org API.

## Resources

It has a couple of extra plist, where info has been saved, for example:

- URLRequest: Where all the HTTP required have been saved. Included the api_key.

## Tests

In the folder PopcornTimeTests there are couple of classes where you can find the unit tests done.

## Production

All the Folders, Classes, Functions, Properties and Constat values have been documented, well formatted and created with clear names. 
Following a clean code process where makes the code readable and ready for production. 
Cover by UnitTests the main functionality.