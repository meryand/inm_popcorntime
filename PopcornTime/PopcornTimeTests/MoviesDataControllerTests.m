//
//  MoviesDataControllerTests.m
//  PopcornTime
//
//  Created by Ortega, Maria on 02/07/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MoviesDataController.h"
#import "Movie.h"

@interface MoviesDataControllerTests : XCTestCase
typedef void(^TestNamesCompletionBlock)(NSArray *testNames);
typedef void(^TestNamesFailureBlock)(NSError *error);

@end

@implementation MoviesDataControllerTests {
    MoviesDataController *dataController;
    NSArray *input;
}

- (void)setUp {
    [super setUp];
    dataController = [MoviesDataController new];
    
    NSArray *moviesArray = @[ @{
                                  @"title": @"Independence Day: Resurgence",
                                  @"popularity": @37.948586,
                                  @"poster_path": @"/9KQX22BeFzuNM66pBA6JbiaJ7Mi.jpg",
                                  @"release_date": @"2016-06-22",
                                  @"vote_average" : @4.55,
                                  @"genre_ids" : @[ @28, @12, @878],
                                  @"backdrop_path": @"/8SqBiesvo1rh9P1hbJTmnVum6jv.jpg"
                                  }, @{
                                  @"title": @"Batman v Superman: Dawn of Justice",
                                  @"popularity": @30.463327,
                                  @"poster_path": @"/cGOPbv9wA5gEejkUN892JrveARt.jpg",
                                  @"release_date": @"2016-03-23",
                                  @"vote_average" : @5.64,
                                  @"genre_ids" : @[ @28, @12, @14],
                                  @"backdrop_path": @"/vsjBeMPZtyB7yNsYY56XYxifaQZ.jpg"
                                  }, @{
                                  @"title": @"X-Men: Apocalypse",
                                  @"popularity": @26.689809,
                                  @"poster_path": @"/zSouWWrySXshPCT4t3UKCQGayyo.jpg",
                                  @"release_date": @"2016-05-18",
                                  @"vote_average" : @5.99,
                                  @"genre_ids" : @[ @28, @12, @878, @14],
                                  @"backdrop_path": @"/oQWWth5AOtbWG9o8SCAviGcADed.jpg"
                                  }];
    
    Movie *movie1 = [[Movie alloc] initWithDictionary:moviesArray[0]];
    Movie *movie2 = [[Movie alloc] initWithDictionary:moviesArray[1]];
    Movie *movie3 = [[Movie alloc] initWithDictionary:moviesArray[2]];
    
    input = @[movie1, movie2, movie3];
}

- (void)tesDataController_ShouldGetsMoviesArrayFromNetworkOperations {
    [dataController moviesData:^(NSArray *movies) {
        TestNamesCompletionBlock completionBlock = [movies objectAtIndex:0];
        XCTAssert(movies, @"movies nil");
        XCTAssertNotNil(input);
        completionBlock(input);
    } failureBlock:^(NSError *error) {
        TestNamesFailureBlock failureBlock;
        failureBlock(error);
    } sender:nil];
}

@end
