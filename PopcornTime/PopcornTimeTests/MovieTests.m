//
//  MovieTests.m
//  PopcornTime
//
//  Created by Ortega, Maria on 01/07/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Movie.h"
#import "GenreParse.h"
#import "DateFormat.h"

@interface GenreParse (Tests)
+ (NSString *)generNamesForIds:(NSArray *)genreIDs;
@end

@interface DateFormat (Tests)
+ (NSString *)formatDate:(NSString *)date;
@end

@interface MovieTests : XCTestCase

@end

@implementation MovieTests {
    Movie *movie1;
    Movie *movie2;
    Movie *movie3;
    NSArray *moviesArray;
}

- (void)setUp {
    [super setUp];
    moviesArray = @[ @{
                            @"title": @"Independence Day: Resurgence",
                            @"popularity": @37.948586,
                            @"poster_path": @"/9KQX22BeFzuNM66pBA6JbiaJ7Mi.jpg",
                            @"release_date": @"2016-06-22",
                            @"vote_average" : @4.55,
                            @"genre_ids" : @[ @28, @12, @878],
                            @"backdrop_path": @"/8SqBiesvo1rh9P1hbJTmnVum6jv.jpg"
                            }, @{
                            @"title": @"Batman v Superman: Dawn of Justice",
                            @"popularity": @30.463327,
                            @"poster_path": @"/cGOPbv9wA5gEejkUN892JrveARt.jpg",
                            @"release_date": @"2016-03-23",
                            @"vote_average" : @5.64,
                            @"genre_ids" : @[ @28, @12, @14],
                            @"backdrop_path": @"/vsjBeMPZtyB7yNsYY56XYxifaQZ.jpg"
                            }, @{
                            @"title": @"X-Men: Apocalypse",
                            @"popularity": @26.689809,
                            @"poster_path": @"/zSouWWrySXshPCT4t3UKCQGayyo.jpg",
                            @"release_date": @"2016-05-18",
                            @"vote_average" : @5.99,
                            @"genre_ids" : @[ @28, @12, @878, @14],
                            @"backdrop_path": @"/oQWWth5AOtbWG9o8SCAviGcADed.jpg"
                            }];

    movie1 = [[Movie alloc] initWithDictionary:moviesArray[0]];
    movie2 = [[Movie alloc] initWithDictionary:moviesArray[1]];
    movie3 = [[Movie alloc] initWithDictionary:moviesArray[2]];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testAMovieModel_ShouldNotBeNil {
    XCTAssertNotNil(movie1);
    XCTAssertNotNil(movie2);
    XCTAssertNotNil(movie3);
}

- (void)testAMovieModel_ShouldHaveValuesForTheProperties {
    XCTAssertNotNil(movie1.title);
    XCTAssertNotNil(movie1.genre);
    XCTAssertNotNil(movie1.releaseYear);
    XCTAssertNotNil(movie1.popularity);
}

- (void)testAMovieModel_ShouldHaveCorrectValuesForTheProperties {
    XCTAssertEqualObjects(movie1.title, moviesArray[0][@"title"]);
    XCTAssertEqualObjects(movie1.popularity, moviesArray[0][@"popularity"]);
    XCTAssertEqualObjects(movie1.releaseYear, moviesArray[0][@"release_date"]);
}

- (void)testAMovieModel_ShouldHaveCorrectValuesForTheGenresIds {
    XCTAssertEqualObjects(movie1.genre, [GenreParse generNamesForIds:moviesArray[0][@"genre_ids"]]);
}

- (void)testAMovieModel_ShouldHaveCorrectValuesForMovieReleaseYear {
    XCTAssertEqualObjects([DateFormat formatDate:movie1.releaseYear], @"22/06/2016");
}

- (void)testAMovieModel_ShouldHaveCorrectValuesForThenJsonDataReleaseYear {
    XCTAssertEqualObjects([DateFormat formatDate:moviesArray[0][@"release_date"]], @"22/06/2016");
}

@end
