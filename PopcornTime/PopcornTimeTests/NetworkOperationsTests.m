//
//  NetworkOperationsTests.m
//  PopcornTime
//
//  Created by Ortega, Maria on 01/07/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NetworkOperations.h"
#import <AFNetworking/AFNetworking.h>

@interface NetworkOperationsTests : XCTestCase

@end

@implementation NetworkOperationsTests

- (void)tearDown {
    [super tearDown];
}

- (void)testNetworkOperations_ShouldFetchARemoteImageWithURLGiven {
    XCTestExpectation *expectation = [self expectationWithDescription:@"asynchronous request"];
    
    __block UIImage *image = nil;
    NSURL *url = [NSURL URLWithString:@"https://image.tmdb.org/t/p/w92/8SqBiesvo1rh9P1hbJTmnVum6jv.jpg"];
    NSURLSession *session = [NSURLSession sharedSession];

    NSURLSessionTask *task = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        XCTAssertNil(error, @"dataTaskWithURL error %@", error);
        
        if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
            NSInteger statusCode = [(NSHTTPURLResponse *) response statusCode];
            XCTAssertEqual(statusCode, 200, @"status code was not 200; was %ld", (long)statusCode);
        }
        
        XCTAssert(data, @"data nil");
        image = [UIImage imageWithData:data];
        
        XCTAssertNotNil(image);        
        [expectation fulfill];
    }];
    [task resume];
    
    [self waitForExpectationsWithTimeout:30.0 handler:nil];
}

- (void)testNetworkOperations_ShouldRequestGenresMovieList {
    XCTestExpectation *expectation = [self expectationWithDescription:@"asynchronous request"];
    
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"genresTest" ofType:@"plist"];
    
    NSURL *url = [NSURL URLWithString:@"https://api.themoviedb.org/3/genre/movie/list?api_key=3ca2d671da9f1fdef94ec6301471635a"];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
                                                XCTAssertNil(error, @"dataTaskWithURL error %@", error);

                                                
                                                if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                                    NSInteger statusCode = [(NSHTTPURLResponse *) response statusCode];
                                                    XCTAssertEqual(statusCode, 200, @"status code was not 200; was %ld", (long)statusCode);
                                                }
                                                
                                                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                
                                                data =  [NSPropertyListSerialization dataWithPropertyList:dict
                                                                                                   format:NSPropertyListXMLFormat_v1_0
                                                                                                  options:NSPropertyListMutableContainersAndLeaves
                                                                                                    error:nil];
                                                if(data != nil) {
                                                    [data writeToFile:bundlePath atomically:YES];
                                                }
                                                
                                                XCTAssertNotNil(data);
                                                [expectation fulfill];
                                            }];
    [task resume];
    
    [self waitForExpectationsWithTimeout:50.0 handler:nil];
}

@end
