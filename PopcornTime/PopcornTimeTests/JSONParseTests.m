//
//  JSONParseTests.m
//  PopcornTime
//
//  Created by Ortega, Maria on 01/07/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "JSONParse.h"
#import "Movie.h"

@interface JSONParseTests : XCTestCase

@end

@implementation JSONParseTests {
    NSArray *input;
    NSArray *ouput;
}

- (void)setUp {
    [super setUp];
    NSDictionary *moviesDict = @{ @"results" : @[ @{
                                  
                                  @"title": @"Independence Day: Resurgence",
                                  @"popularity": @37.948586,
                                  @"poster_path": @"/9KQX22BeFzuNM66pBA6JbiaJ7Mi.jpg",
                                  @"release_date": @"2016-06-22",
                                  @"vote_average" : @4.55,
                                  @"genre_ids" : @[ @28, @12, @878],
                                  @"backdrop_path": @"/8SqBiesvo1rh9P1hbJTmnVum6jv.jpg"
                                  }, @{
                                  @"title": @"Batman v Superman: Dawn of Justice",
                                  @"popularity": @30.463327,
                                  @"poster_path": @"/cGOPbv9wA5gEejkUN892JrveARt.jpg",
                                  @"release_date": @"2016-03-23",
                                  @"vote_average" : @5.64,
                                  @"genre_ids" : @[ @28, @12, @14],
                                  @"backdrop_path": @"/vsjBeMPZtyB7yNsYY56XYxifaQZ.jpg"
                                  }, @{
                                  @"title": @"X-Men: Apocalypse",
                                  @"popularity": @26.689809,
                                  @"poster_path": @"/zSouWWrySXshPCT4t3UKCQGayyo.jpg",
                                  @"release_date": @"2016-05-18",
                                  @"vote_average" : @5.99,
                                  @"genre_ids" : @[ @28, @12, @878, @14],
                                  @"backdrop_path": @"/oQWWth5AOtbWG9o8SCAviGcADed.jpg"
                                  }]};
    
    NSArray *moviesArray = @[ @{
                         @"title": @"Independence Day: Resurgence",
                         @"popularity": @37.948586,
                         @"poster_path": @"/9KQX22BeFzuNM66pBA6JbiaJ7Mi.jpg",
                         @"release_date": @"2016-06-22",
                         @"vote_average" : @4.55,
                         @"genre_ids" : @[ @28, @12, @878],
                         @"backdrop_path": @"/8SqBiesvo1rh9P1hbJTmnVum6jv.jpg"
                         }, @{
                         @"title": @"Batman v Superman: Dawn of Justice",
                         @"popularity": @30.463327,
                         @"poster_path": @"/cGOPbv9wA5gEejkUN892JrveARt.jpg",
                         @"release_date": @"2016-03-23",
                         @"vote_average" : @5.64,
                         @"genre_ids" : @[ @28, @12, @14],
                         @"backdrop_path": @"/vsjBeMPZtyB7yNsYY56XYxifaQZ.jpg"
                         }, @{
                         @"title": @"X-Men: Apocalypse",
                         @"popularity": @26.689809,
                         @"poster_path": @"/zSouWWrySXshPCT4t3UKCQGayyo.jpg",
                         @"release_date": @"2016-05-18",
                         @"vote_average" : @5.99,
                         @"genre_ids" : @[ @28, @12, @878, @14],
                         @"backdrop_path": @"/oQWWth5AOtbWG9o8SCAviGcADed.jpg"
                         }];

    
    Movie *movie1 = [[Movie alloc] initWithDictionary:moviesArray[0]];
    Movie *movie2 = [[Movie alloc] initWithDictionary:moviesArray[1]];
    Movie *movie3 = [[Movie alloc] initWithDictionary:moviesArray[2]];
    
    input = @[movie1, movie2, movie3];
    
    ouput = [JSONParse dataFromJsonFile:moviesDict];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testParserJson_ShouldHaveTheSameNumberOfMovies {
    XCTAssertEqual(input.count, ouput.count);
}

- (void)testParserJson_ShouldHaveTheSameMoviesTitle {
    XCTAssertEqualObjects([input[0] title], [ouput[0] title]);
    XCTAssertEqualObjects([input[1] title], [ouput[1] title]);
    XCTAssertEqualObjects([input[2] title], [ouput[2] title]);
}

- (void)testParserJson_ShouldHaveTheSamePopularity {
    XCTAssertEqualObjects([input[0] popularity], [ouput[0] popularity]);
    XCTAssertEqualObjects([input[1] popularity], [ouput[1] popularity]);
    XCTAssertEqualObjects([input[2] popularity], [ouput[2] popularity]);
}

- (void)testParserJson_ShouldHaveTheSameaverage {
    XCTAssertEqualObjects([input[0] average], [ouput[0] average]);
    XCTAssertEqualObjects([input[1] average], [ouput[1] average]);
    XCTAssertEqualObjects([input[2] average], [ouput[2] average]);
}

@end
