//
//  HomeViewController.m
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "HomeViewController.h"
#import "MoviesTableViewController.h"
#import "MoviesDataController.h"

static NSString *const storyboardMoviesListIdentifier = @"showListOfMovies";

@interface HomeViewController ()
@property (nonatomic, strong) NSArray *moviesList;
@property (nonatomic, strong) MoviesDataController *dataController;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataController = [MoviesDataController new];
    [self.dataController requestGenresMovieList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Request Movies Data

- (void)requestMoviesData:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    [self.dataController moviesData:^(NSArray *movies) {
        weakSelf.moviesList = movies;
        if (weakSelf.moviesList) {
            [weakSelf performSegueWithIdentifier:storyboardMoviesListIdentifier sender:sender];
        }
    } failureBlock:^(NSError *error) {
    } sender:sender];
}

#pragma mark - Segue Logic

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if (self.moviesList) {
        return YES;
    } else {
        [self requestMoviesData:sender];
        return NO;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:storyboardMoviesListIdentifier]) {
        MoviesTableViewController *viewController = [segue destinationViewController];
        viewController.movies = self.moviesList;
    }
}

@end
