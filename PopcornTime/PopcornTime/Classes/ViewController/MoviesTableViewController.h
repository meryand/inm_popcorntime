//
//  MoviesTableViewController.h
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoviesTableViewController : UITableViewController

/**
 *  Array of movies to be shown in the TableView
 */
@property (strong, nonatomic) NSArray *movies;

@end
