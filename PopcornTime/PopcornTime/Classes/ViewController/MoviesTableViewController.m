//
//  MoviesTableViewController.m
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "MoviesTableViewController.h"
#import "MovieDetailsTableViewCell.h"
#import "MovieDetailsViewModel.h"
#import "DetailsMovieViewController.h"

static NSString *const customerCellIdentifier = @"movieCellIdentifier";
static NSString *const storyboardDetailsMovieIdentifier = @"showDetailsMovie";

@implementation MoviesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.pagingEnabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table View Logic

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.movies.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:customerCellIdentifier forIndexPath:indexPath];
    MovieDetailsViewModel *viewModel = [MovieDetailsViewModel viewModelWithModel:self.movies[indexPath.row]];
    [cell updateWithViewModel:viewModel];
    return cell;
}

#pragma mark - Segue Logic

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:storyboardDetailsMovieIdentifier]) {
        NSIndexPath *index = [self.tableView indexPathForCell:(MovieDetailsTableViewCell *)sender];
        DetailsMovieViewController *detailMovie = [segue destinationViewController];
        detailMovie.detailViewModel = self.movies[index.row];
    }
}

@end
