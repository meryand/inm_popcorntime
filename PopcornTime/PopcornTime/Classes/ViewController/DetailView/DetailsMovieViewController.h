//
//  DetailsMovieViewController.h
//  PopcornTime
//
//  Created by Ortega, Maria on 01/07/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieDetailsViewModel.h"

@interface DetailsMovieViewController : UIViewController

@property(nonatomic, strong) MovieDetailsViewModel *detailViewModel;

/**
 *  Movie Poster Image
 */
@property (weak, nonatomic) IBOutlet UIImageView *poster;

/**
 *  Movie Title Label
 */
@property (weak, nonatomic) IBOutlet UILabel *movieTitle;

/**
 *  Movie Genre Label
 */
@property (weak, nonatomic) IBOutlet UILabel *genre;

/**
 *  Movie Popularity Label
 */
@property (weak, nonatomic) IBOutlet UILabel *popularity;

/**
 *  Movie rate average Label
 */
@property (weak, nonatomic) IBOutlet UILabel *average;

/**
 *  Movie Overview text
 */
@property (weak, nonatomic) IBOutlet UITextView *overiview;

@end
