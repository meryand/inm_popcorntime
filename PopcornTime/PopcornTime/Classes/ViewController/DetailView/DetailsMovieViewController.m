//
//  DetailsMovieViewController.m
//  PopcornTime
//
//  Created by Ortega, Maria on 01/07/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "DetailsMovieViewController.h"
#import "DateFormat.h"
#import "NetworkOperations.h"

@interface DetailsMovieViewController()
@property (nonatomic, strong) NetworkOperations *operation;
@end

@implementation DetailsMovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.detailViewModel.title;
    [self setupViews];
}

- (void)setupViews {
    self.movieTitle.text = [NSString stringWithFormat:@"%@ (%@)", self.detailViewModel.title, [DateFormat formatDate:self.detailViewModel.releaseYear]];
    self.genre.text = self.detailViewModel.genre;
    self.popularity.text = [NSString stringWithFormat:@"%@", self.detailViewModel.popularity];
    self.average.text = [NSString stringWithFormat:@"%@", self.detailViewModel.average];
    self.overiview.text = self.detailViewModel.overview;
    [self fetchRemoteImageWithURL:self.detailViewModel.poster];
    [self.view reloadInputViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)fetchRemoteImageWithURL:(NSString *)stringURL {
    [NetworkOperations fetchRemoteImageWithURL:stringURL
                                  successBlock:^(UIImage *image) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          self.poster.image = image;
                                      });
                                  }
                                  failureBlock:^(NSError *error) {
                                  }];
}

@end
