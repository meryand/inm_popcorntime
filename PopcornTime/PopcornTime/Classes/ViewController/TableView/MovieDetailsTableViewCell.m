//
//  MovieDetailsTableViewCell.m
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "MovieDetailsTableViewCell.h"
#import "NetworkOperations.h"

@interface MovieDetailsTableViewCell()
@property (nonatomic, strong) MovieDetailsViewModel *viewModel;
@end

@implementation MovieDetailsTableViewCell

- (void)updateWithViewModel:(MovieDetailsViewModel *)viewModel {
    self.title.text = viewModel.title;
    self.genre.text = viewModel.genre;
    self.popularity.text = [NSString stringWithFormat:@"%@", viewModel.popularity];
    self.average.text = [NSString stringWithFormat:@"%@", viewModel.average];

    [self fetchRemoteImageWithURL:viewModel.thumbnail];
}

- (void)fetchRemoteImageWithURL:(NSString *)stringURL {
    [NetworkOperations fetchRemoteImageWithURL:stringURL
                                  successBlock:^(UIImage *image) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          self.thumbnail.image = image;
                                      });
                                  }
                                  failureBlock:^(NSError *error) {
                                  }];
}

@end
