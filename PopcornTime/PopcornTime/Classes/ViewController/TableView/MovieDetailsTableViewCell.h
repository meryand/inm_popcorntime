//
//  MovieDetailsTableViewCell.h
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieDetailsViewModel.h"

@interface MovieDetailsTableViewCell : UITableViewCell

/**
 *  Movie Title Label
 */
@property (weak, nonatomic) IBOutlet UILabel *title;

/**
 *  Movie Genre Label
 */
@property (weak, nonatomic) IBOutlet UILabel *genre;

/**
 *  Thumbnail Genre Image
 */
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;

/**
 *  Popularity Label
 */
@property (weak, nonatomic) IBOutlet UILabel *popularity;

/**
 *  Average Label
 */
@property (weak, nonatomic) IBOutlet UILabel *average;

/**
 *  Poster image
 */
@property (strong, nonatomic) UIImage *poster;

/**
 *  Set the viewModel used for Movie model
 *
 *  @param viewModel a MovieDetailsViewModel
 */
- (void)updateWithViewModel:(MovieDetailsViewModel *)viewModel;

@end
