//
//  JSONParse.h
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONParse : NSObject

/**
 *  JSON Parser froma a file
 *
 *  @param fileName the name of the json file
 *
 *  @return array of customer models parsed
 */
+ (NSArray *)dataFromJsonFile:(NSDictionary *)data;

@end
