//
//  JSONParse.m
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "JSONParse.h"
#import "Movie.h"

static NSString *const resultsIdentifier = @"results";

@implementation JSONParse

+ (NSArray *)dataFromJsonFile:(NSDictionary *)data {
    
    NSMutableArray *moviesList = [NSMutableArray new];
    NSArray *results = data[resultsIdentifier];
    
    if (results != nil) {
        for (NSDictionary *element in results) {
            Movie *movie = [[Movie alloc] initWithDictionary:element];
            [moviesList addObject:movie];
        }
    }
    return moviesList.copy;
}

@end
