//
//  DateFormat.h
//  PopcornTime
//
//  Created by Ortega, Maria on 01/07/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateFormat : NSObject

/**
 *  Date Formater
 *
 *  @param date the date as a string
 *
 *  @return string with the date in the correct format DD/MM/YYYY
 */
+ (NSString *)formatDate:(NSString *)date;

@end
