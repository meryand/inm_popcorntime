//
//  DateFormat.m
//  PopcornTime
//
//  Created by Ortega, Maria on 01/07/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "DateFormat.h"

@implementation DateFormat

+ (NSString *)formatDate:(NSString *)date {
    NSString *correctDate;
    NSArray *dateArray = [date componentsSeparatedByString: @"-"];
    
    if (dateArray.count > 2) {
        correctDate = [NSString stringWithFormat:@"%@/%@/%@", dateArray[2], dateArray[1], dateArray[0]];
    }
    return correctDate;
}

@end
