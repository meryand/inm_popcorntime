//
//  GenreParse.m
//  PopcornTime
//
//  Created by Ortega, Maria on 01/07/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "GenreParse.h"

static NSString *const idIdentifier = @"id";
static NSString *const nameIdentifier = @"name";
static NSString *const genresIdentifier = @"genres";
static NSString *const plistIdentifier = @"plist";
static NSString *const resultsIdentifier = @"results";

@implementation GenreParse

+ (NSString *)generNamesForIds:(NSArray *)genreIDs {
    NSString *movieGenres = @"";
    NSString *filePath = [[NSBundle mainBundle] pathForResource:genresIdentifier ofType:plistIdentifier];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    NSArray *array = [NSArray arrayWithArray:[dict objectForKey:genresIdentifier]];
    
    for (NSDictionary *genre in array) {
        if ([genreIDs containsObject:genre[idIdentifier]]) {
            movieGenres = [movieGenres  isEqualToString:@""] ? [NSString stringWithFormat:@"%@%@", movieGenres, genre[nameIdentifier]] : [NSString stringWithFormat:@"%@, %@", movieGenres, genre[nameIdentifier]];
        }
    }
    return movieGenres;
}

@end
