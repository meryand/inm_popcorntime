//
//  GenreParse.h
//  PopcornTime
//
//  Created by Ortega, Maria on 01/07/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GenreParse : NSObject

/**
 *  Genre Parser froma an array
 *
 *  @param genreIDs the array of genre IDs of
 *
 *  @return string, genre name or names for that ID
 */
+ (NSString *)generNamesForIds:(NSArray *)genreIDs;

@end
