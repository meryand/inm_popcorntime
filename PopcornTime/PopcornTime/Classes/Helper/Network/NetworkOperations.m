//
//  NetworkOperations.m
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "NetworkOperations.h"
#import "AFNetworking.h"
#import "Movie.h"
#import "JSONParse.h"

static NSString *const urlRequestIdentifier = @"URLRequest";
static NSString *const discoverMoviesIdentifier = @"discoverMovies";
static NSString *const apiKeyIdentifier = @"apiKey";
static NSString *const genreListIdentifier = @"genreList";
static NSString *const genresIdentifier = @"genres";
static NSString *const plistIdentifier = @"plist";
static NSString *const resultsIdentifier = @"results";
static NSString *const imagesURLIdentifier = @"imagesURL";

@interface NetworkOperations()
@property (nonatomic, strong) NSOperation *moviesOperation;
@end

@implementation NetworkOperations

+ (NSOperation *)moviesListOperationWithSuccess:(void (^)(NSArray *movies))completionBlock
                                        failure:(void (^)(NSError *error, NSInteger errorCode))failureBlock {

    NSString *filePath = [[NSBundle mainBundle] pathForResource:urlRequestIdentifier ofType:plistIdentifier];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    NSString *urlString = [NSString stringWithFormat:@"%@&%@",dict[discoverMoviesIdentifier], dict[apiKeyIdentifier]];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess: ^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                             options:kNilOptions
                                                               error:&error];
        NSArray *moviesList = [JSONParse dataFromJsonFile:json];
        
        if (completionBlock) {
            completionBlock(moviesList);
        }
    } failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failureBlock) {
            failureBlock(error, operation.response.statusCode);
        }
    }];
    
    return operation;
}

+ (void)requestGenresMovieList {
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:genresIdentifier ofType:plistIdentifier];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:urlRequestIdentifier ofType:plistIdentifier];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    NSString *urlString = [NSString stringWithFormat:@"%@?%@",dict[genreListIdentifier], dict[apiKeyIdentifier]];

    NSURL *URL = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
                                                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

                                          data =  [NSPropertyListSerialization dataWithPropertyList:dict
                                                                                             format:NSPropertyListXMLFormat_v1_0
                                                                                            options:NSPropertyListMutableContainersAndLeaves
                                                                                              error:nil];
                                          if(data != nil) {
                                              [data writeToFile:bundlePath atomically:YES];
                                          }
                                      }];
        [task resume];
}


- (void)requestMoviesData:(void (^)(NSArray *movies))completionBlock
             failureBlock:(void (^)(NSError *error))failureBlock
                   sender:(id)sender {
    [self.moviesOperation cancel];
    self.moviesOperation = [NetworkOperations moviesListOperationWithSuccess:^(NSArray *movies) {
        if (completionBlock) {
            completionBlock(movies);
        }
    } failure:^(NSError *error, NSInteger errorCode) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
    [self.moviesOperation start];
}

+ (void)fetchRemoteImageWithURL:(NSString *)stringURL
                   successBlock:(void (^)(UIImage *image))completionBlock
                   failureBlock:(void (^)(NSError *error))failureBlock {
    __block UIImage *image = nil;
    NSString *imageFullURL = [NetworkOperations imageStringURLForImage:stringURL];
    NSURL *imageURL = [NSURL URLWithString:imageFullURL];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *task = [session dataTaskWithURL:imageURL
                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                            
                                            image = [UIImage imageWithData:data];
                                            if (!image) {
                                                image = nil;
                                            }
                                            if (completionBlock) {
                                                completionBlock(image);
                                            } else if (failureBlock) {
                                                failureBlock(error);
                                            }
                                        }];
    [task resume];
}

+ (NSString *)imageStringURLForImage:(NSString *)image {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:urlRequestIdentifier ofType:plistIdentifier];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    return  [NSString stringWithFormat:@"%@%@",dict[imagesURLIdentifier],image];
}


@end
