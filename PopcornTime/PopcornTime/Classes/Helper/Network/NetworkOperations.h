//
//  NetworkOperations.h
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NetworkOperations : NSObject

/**
 *  Operation to get the list of movies
 *
 *  @param completion Block with the arrays of movies
 *  @param failure Block with the error
 *
 *  @return opertion of requesting the list ofm ovies
 */
+ (NSOperation *)moviesListOperationWithSuccess:(void (^)(NSArray *movies))completionBlock
                                        failure:(void (^)(NSError *error, NSInteger errorCode))failureBlock;

/**
 *  Request movies data
 *
 *  @param completion Block with the arrays of movies
 *  @param failure Block with the error
 *  @param sender
 *
 *  @return opertion of requesting the list ofm ovies
 */
- (void)requestMoviesData:(void (^)(NSArray *movies))completionBlock
             failureBlock:(void (^)(NSError *error))failureBlock
                   sender:(id)sender;

/**
 *  Request genres movies list
 */
+ (void)requestGenresMovieList;

/**
 *  Fetch the remote image url
 *
 *  @param string URL
 *  @param completion Block with the image
 *  @param failure Block with the error
 *
 *  @return opertion of requesting the list ofm ovies
 */
+ (void)fetchRemoteImageWithURL:(NSString *)stringURL
                   successBlock:(void (^)(UIImage *image))completionBlock
                   failureBlock:(void (^)(NSError *error))failureBlock;

@end
