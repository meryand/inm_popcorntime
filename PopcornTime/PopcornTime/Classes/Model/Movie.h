//
//  Movie.h
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject

/**
 *  Movie Title
 */
@property (strong, nonatomic) NSString *title;

/**
 *  Movie Genre
 */
@property (strong, nonatomic) NSString *genre;

/**
 *  Movie Release Year
 */
@property (strong, nonatomic) NSString *releaseYear;

/**
 *  Movie thumbnail
 */
@property (strong, nonatomic) NSString *thumbnail;

/**
 *  Movie Popularity
 */
@property (strong, nonatomic) NSNumber *popularity;

/**
 *  Movie Vote Average
 */
@property (strong, nonatomic) NSNumber *average;

/**
 *  Movie Poster
 */
@property (strong, nonatomic) NSString *poster;

/**
 *  Movie Overview
 */
@property (strong, nonatomic) NSString *overview;

/**
 *  The designated initializer
 *
 *  @param dictionary a dictionary
 *  @return an instance of model
 */
- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
