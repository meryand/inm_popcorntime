//
//  Movie.m
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "Movie.h"
#import "GenreParse.h"

@implementation Movie

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if ([dictionary isKindOfClass:[NSDictionary class]]) {
            [self setValuesForKeysWithDictionary:dictionary];
        }
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key {
    if ([key isEqualToString:@"title"]) {
        self.title= value;
    } else if ([key isEqualToString:@"genre_ids"]) {
        self.genre = [GenreParse generNamesForIds:value] ;
    } else if ([key isEqualToString:@"release_date"]) {
        self.releaseYear = value;
    } else if ([key isEqualToString:@"backdrop_path"]) {
        self.thumbnail = value;
    } else if ([key isEqualToString:@"popularity"]) {
        self.popularity = value;
    } else if ([key isEqualToString:@"vote_average"]) {
        self.average = value;
    } else if ([key isEqualToString:@"poster_path"]) {
        self.poster = value;
    } else if ([key isEqualToString:@"overview"]) {
        self.overview = value;
    }
}

@end
