//
//  MovieDetailsViewModel.h
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Movie.h"
#import <UIKit/UIKit.h>

@interface MovieDetailsViewModel : NSObject

/**
 *  Movie Title
 */
@property (strong, nonatomic) NSString *title;

/**
 *  Movie Genre
 */
@property (strong, nonatomic) NSString *genre;

/**
 *  Thumbnail Genre
 */
@property (strong, nonatomic) NSString *thumbnail;

/**
 *  Release Year
 */
@property (strong, nonatomic) NSString *releaseYear;

/**
 *  Movie Popularity
 */
@property (strong, nonatomic) NSNumber *popularity;

/**
 *  Movie Vote Average
 */
@property (strong, nonatomic) NSNumber *average;

/**
 *  Movie Poster
 */
@property (strong, nonatomic) NSString *poster;

/**
 *  Movie Overview
 */
@property (strong, nonatomic) NSString *overview;

/**
 *  Movie Full Title
 */
@property (strong, nonatomic) NSString *titleWithReleaseYear;

/**
 *  Creates a new instance
 *
 *  @param data model a Movie model
 *
 *  @return A new instance
 */
+ (instancetype)viewModelWithModel:(Movie *)model;




@end
