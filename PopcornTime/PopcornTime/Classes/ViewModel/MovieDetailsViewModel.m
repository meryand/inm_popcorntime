//
//  MovieDetailsViewModel.m
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "MovieDetailsViewModel.h"
#import "DateFormat.h"

@interface MovieDetailsViewModel()
@property (nonatomic, strong) Movie *model;
@end

@implementation MovieDetailsViewModel
@synthesize titleWithReleaseYear;

+ (instancetype)viewModelWithModel:(Movie *)model {
    return [[self alloc] initWithModel:model];
}

- (instancetype)initWithModel:(Movie *)model {
    self = [super init];
    if (self) {
        _model = model;
        _releaseYear = model.releaseYear;
        _genre = model.genre;
        _thumbnail = model.thumbnail;
        _poster = model.poster;
        _popularity = model.popularity;
        _average = model.average;
        _overview = model.overview;
        _title = self.titleWithReleaseYear;
    }
    return self;
}

- (NSString *)titleWithReleaseYear {
    return [NSString stringWithFormat:@"%@ (%@)", self.model.title, [DateFormat formatDate:self.releaseYear]];
}

@end
