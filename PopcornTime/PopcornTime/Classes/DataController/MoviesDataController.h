//
//  MoviesDataController.h
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MoviesDataController : NSObject

/**
 *  Return the movies array after beeing parsed, sorted and filtered
 *
 *  @param completion Block with the arrays of movies
 *  @param failure Block with the error
 *  @param sender
 *
 *  @return opertion of requesting the list ofm ovies
 */
- (void)moviesData:(void (^)(NSArray *movies))completionBlock
      failureBlock:(void (^)(NSError *error))failureBlock
            sender:(id)sender;

/**
 *  Do the request the list of the genres IDs and store them into a plist
 */
- (void)requestGenresMovieList;

@end
