//
//  MoviesDataController.m
//  PopcornTime
//
//  Created by Ortega, Maria on 30/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "MoviesDataController.h"
#import "NetworkOperations.h"

@interface MoviesDataController()
@property (nonatomic, strong) NetworkOperations *operation;
@end

@implementation MoviesDataController

- (void)moviesData:(void (^)(NSArray *movies))completionBlock
      failureBlock:(void (^)(NSError *error))failureBlock
            sender:(id)sender {
    self.operation = [NetworkOperations new];
    [self.operation requestMoviesData:^(NSArray *movies) {
        if (completionBlock) {
            completionBlock(movies);
        }
    } failureBlock:^(NSError *error) {
        if  (failureBlock) {
            failureBlock(error);
        }
    } sender:sender];
}

- (void)requestGenresMovieList {
    [NetworkOperations requestGenresMovieList];
}

@end
